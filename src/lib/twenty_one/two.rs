use std::{
    fs::File,
    io::{BufRead, BufReader},
};

pub fn two_a() {
    let file = File::open("./input_2.txt").expect("Can't read file");
    let reader = BufReader::new(file);

    let mut x = 0;
    let mut y = 0;

    for line in reader.lines() {
        let line = line.expect("Something is wrong in lines");
        let split_line = line.split(" ").collect::<Vec<&str>>();
        let direction = split_line[0];
        let amount = split_line[1];

        if direction == "forward" {
            x += amount.parse::<i32>().expect("Cant parse this number :(");
        } else {
            y += match direction {
                "up" => -amount.parse::<i32>().expect("Cant parse this number :("),
                "down" => amount.parse::<i32>().expect("Cant parse this number :("),
                _ => 0,
            }
        }
    }

    let mutli = x * y;
    println!("Multi: {:?}", mutli);
}

